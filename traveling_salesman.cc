// Copyright © 2021 Neven Sajko. All rights reserved.

#include <algorithm>
#include <array>
#include <bit>
#include <bitset>
#include <concepts>
#include <cstdint>
#include <iostream>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace tsp {

// The global optimum solution to the traveling salesman problem.
template<typename Ind, typename T, std::regular_invocable<Ind, Ind> WF>
auto tsp(Ind num_nodes, WF edge_weight) {
	std::vector<Ind> v(num_nodes);
	for (Ind i{0}; i < v.size(); i++) {
		v[i] = i;
	}

	// v.size() must be greater than 2.
	auto w{[edge_weight](const std::vector<Ind> &v) {
		auto sum{edge_weight(v.front(), v.back())};

		for (Ind i{0}; i < v.size() - 1; i++) {
			sum += edge_weight(v[i], v[i + 1]);
		}

		return sum;
	}};

	auto min{w(v)};

	for (;;) {
		std::ranges::next_permutation(v);

		if (v.front() != 0) {
			break;
		}

		auto t{w(v)};
		if (t < min) {
			min = t;
		}
	}

	return min;
}

// A function object implemented as a matrix.
template<typename Ind, typename T, std::regular_invocable<Ind, Ind> WF>
class WeightMatrix {
	Ind num_nodes;
	std::vector<T> m;

public:

	WeightMatrix(Ind num_nodes, WF edge_weight): num_nodes{num_nodes}, m(num_nodes * num_nodes) {
		for (Ind i{0}; i < num_nodes; i++) {
			for (Ind j{0}; j < num_nodes; j++) {
				m[i*num_nodes + j] = edge_weight(i, j);
			}
		}

		//for (Ind i{0}; i < num_nodes; i++) {
		//	for (Ind j{0}; j < num_nodes; j++) {
		//		std::cout <<  m[i*num_nodes + j] << ' ';
		//	}
		//	std::cout << '\n';
		//}
		//std::cout << '\n';
	}

	auto operator()(Ind i, Ind j) const -> T {
		return m[i*num_nodes + j];
	}
};

template<typename Ind, typename T, std::regular_invocable<Ind, Ind> WF>
auto tsp_with_matrix(Ind num_nodes, WF edge_weight) -> T {
	return tsp<Ind, T>(num_nodes, WeightMatrix<Ind, T, WF>{num_nodes, edge_weight});
}

}  // namespace tsp

namespace {

template<typename Ind, typename T>
auto WeightAsDefined(Ind i, Ind j, T a, T b) -> T {
	if (i == j) {
		return 0;
	}
	if (j < i) {
		return WeightAsDefined<Ind, T>(j, i, a, b);
	}
	i++;
	j++;
	auto t{a*i + b*j};
	return t*t + 1;
}

auto exhaustive(int n, long a, long b) -> long {
	return tsp::tsp_with_matrix<int, long>(n, [a, b](int i, int j) -> long {
		return WeightAsDefined<int, long>(i, j, a, b);
	});
}

}  // namespace

extern "C"
auto traveling_salesman(int n, long a, long b) -> long {
	return exhaustive(n, a, b);
}
