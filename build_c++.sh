#! /bin/sh

set -u

cxx_opt='-std=c++2b -pedantic -pedantic-errors -Wall -Wextra -Wold-style-cast -g -march=native -flto -fno-math-errno -ffp-contract=fast -O3'

bindir=/tmp/fricas_guessing_bin

mkdir "$bindir"

IFS=' '
clang++ $cxx_opt -shared -fPIC traveling_salesman.cc -o "$bindir"/tsp.so
